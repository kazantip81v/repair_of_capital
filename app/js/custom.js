$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded');
});

/* viewport width */
function viewport(){
	var e = window, 
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */

$(function(){
	/* placeholder */	   
	$('input, textarea').each(function(){
		var placeholder = $(this).attr('placeholder');
		$(this).focus(function(){ $(this).attr('placeholder', '');});
		$(this).focusout(function(){			 
			$(this).attr('placeholder', placeholder);  			
		});
	});
	/* placeholder */


	/* mobile nav */
	$('.button-nav').click(function(){
		$(this).toggleClass('active'); 
		$('.box-nav').slideToggle();
		return false;
	});
	/* mobile nav */

	$('.js-arrow-down').on('click', function(event) {

		let target = $( $(this).attr('href') );

		if( target.length ) {
				event.preventDefault();
				$('html, body').animate({
						scrollTop: target.offset().top
				}, 1000);
		}
	});

	/* slick slider big */
	if($('.js-slider-big').length) {
		$('.js-slider-big').slick({
			//autoplay: true,
			prevArrow: '<div class="slick-prev"><i class="sprite_white-prev"></i></div>',
			nextArrow: '<div class="slick-next"><i class="sprite_white-next"></div>',
			infinite: true,
			speed: 400,
			centerMode: true,
			centerPadding: '155px',
			slidesToShow: 1,
			asNavFor: '.js-slider-small',
			focusOnSelect: true,
			touchMove: false,
			responsive: [
			{
				breakpoint: 991,
				settings: {
					centerMode: true,
					centerPadding: '100px'
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '60px'
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '20px'
				}
			}
		 ]
		});
	};
	/* slick slider big */


	/* slick slider small */
	if($('.js-slider-small').length) {
		$('.js-slider-small').slick({
			prevArrow: '<div class="slick-prev"><i class="sprite_black-prev"></i></div></div>',
			nextArrow: '<div class="slick-next"><i class="sprite_black-next"></div></div>',
			infinite: false,
			speed: 400,
			slidesToShow: 5,
			slidesToScroll: 1,
			asNavFor: '.js-slider-big',
			focusOnSelect: true,
			touchMove: false,
			responsive: [

				{
					breakpoint: 1440,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1
					}
				}
			]
		});
	};
	/* slick slider small */
	

	/* hide & show - box-slider-small-text*/
	$('.js-slider-small').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  	if (nextSlide !==0) {
	  		$('.box-slider-small-text').addClass('js-text-hide');
	  	} else {
	  		$('.box-slider-small-text').removeClass('js-text-hide');
	  	}
	});
	/* hide & show - box-slider-small-text*/


	/* form styler */
	if($('.styled').length) {
		$('.styled').styler();
	};
	/* form styler */
	

	/* jQuery UI slider - range */
	if($('.js-range').length){
		$('.js-range').slider({
			min: 0,
			max: 500,
			step: 1,
			value: 250,
			create: function( event, ui ) {
				let value = $('.js-range').slider('value');
				$('.point-slider').text('' + value + 'м²');
			},
			slide: function( event, ui ) {
				let value = $('.js-range').slider('value');
				$('.point-slider').text('' + value + 'м²');
			}
		});
	};
	/* jQuery UI slider - range */


	/* fancybox */
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margon  : 10,
			padding  : 10
		});
	};
	/* fancybox */



	/* components */

	/*
	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};
	*/
	
	/* components */

});

var handler = function(){
	
	var height_footer = $('footer').height();	
	var height_header = $('header').height();		
	
	var viewport_width = viewport().width;
	var viewport_height = viewport().height;

	var width = $(window).width();
	var height = $(window).height();
	
	if (width >= 992) {
		$('.box-nav').css('display', '');
	}
	
}
$(window).bind('load', handler);
$(window).bind('resize', handler);



